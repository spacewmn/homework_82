const router = require("express").Router();
const Track = require("../models/Track");

const createRouter = () => {
    router.get("/", async (req, res) => {
        let query;
        if (req.query.album) {
            query = {album: req.query.album}
        }
        try {
            const tracks = await Track.find(query).populate('album');
            res.send(tracks);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    return router;
};

module.exports = createRouter;